#include <iostream>
#include <iterator>
#include <vector>
using namespace std;
int main(){
  vector<int> k;
  k.push_back(7);
  k.push_back(2);
  k.push_back(86);
  k.push_back(11);
  k.push_back(43);
  k.push_back(23);
  //cout<<k[0];
  //cout<<k.size();
  k[3] = 3;
  for (auto i = k.begin(); i != k.end(); ++i) {
    cout<<*(i)<<" ";
  }
  cout<<endl;
  for (auto i = k.cbegin(); i != k.cend(); ++i) {
    cout<<*(i)<<" ";
  }
  cout<<endl;
  for (auto i = k.rbegin(); i != k.rend(); ++i) {
    cout<<*(i)<<" ";
  }
  cout<<endl;
  for (auto i = k.crbegin(); i != k.crend(); ++i) {
    cout<<*(i)<<" ";
  }
  cout<<endl;
  cout<<*(k.begin())<<endl; // returns first element of vector. 
  cout<<k.size()<<endl; // returns the no. of elements.
  cout<<k.capacity()<<endl; // returns the no. of elements the memory of elements  it is holding.
  k.insert(k.begin() + 5,643);
  for (auto i = k.begin(); i != k.end(); ++i) {
    cout<<*(i)<<" ";
  }
}
